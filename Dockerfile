FROM jlesage/baseimage-gui:debian-10-v3

SHELL ["/bin/bash", "-c"]

ENV APP_NAME="Regard3D"

COPY startapp.sh /startapp.sh

RUN apt-get update
RUN apt-get install -y \
git \
p7zip \
cmake \
g++ \
gfortran \
gdb \
valgrind \
qtbase5-dev \
libqt5svg5-dev \
libwxgtk3.0-gtk3-dev \
libboost-dev \
libboost-all-dev \
libopenscenegraph-3.4-dev \
graphviz \
libgsl-dev \
libf2c2-dev \
libfreefem++-dev \
libflann-dev \
libosgearth-dev \
libeigen3-dev \
libopencv-dev \
libceres-dev \
libmetis-dev \
libassimp-dev \
libpng-dev \
libjpeg-dev \
libtiff-dev \
libxxf86vm1 \
libxxf86vm-dev \
libxi-dev \
libxrandr-dev \
libvlfeat-dev 

COPY r3d /r3d
WORKDIR /r3d
ENV R3D="/r3d"

# OpenMVG
RUN mkdir $R3D/openMVG_{Build,Install}

RUN cd $R3D/openMVG_Build && \
cmake -DCMAKE_BUILD_TYPE=RELEASE \
-DOpenMVG_BUILD_EXAMPLES=ON \
-DOpenMVG_BUILD_TESTS=ON \
-DCMAKE_INSTALL_PREFIX:STRING="$R3D/openMVG_Install" \
../openMVG/src/ && \
make -j3 && \
make install

# Regard3D
RUN mkdir $R3D/regard/build
RUN cd $R3D/regard/build && \
cmake -G "Unix Makefiles" \
-j 3 \
-DCMAKE_BUILD_TYPE="Debug" \
-DR3D=$R3D \
-DCERES_DIR=$R3D/openMVG/third_party/ceres-solver \
-DOpenMVG_DIR=$R3D/openMVG_Install/share/openMVG/cmake \
../src

RUN cd $R3D/regard/build && make -j3

# Graclus
RUN cd $R3D/graclus1.2 && make -j3

# CMVS & PMVS2
RUN cd $R3D/cmvs/program/main && make -j3

# Bundler
RUN cd $R3D/bundler-v0.4-source && make -j3

# MVE
RUN cd $R3D/mve && make -j3

# SMVS
RUN cd $R3D/smvs && make -j3

# Poisson Recon
RUN cd $R3D/PoissonRecon && make

# MVS-Texturing
RUN cd $R3D/mvs-texturing && mkdir build && cd build && cmake .. && make -j3

# Installation
# Regard
RUN cd $R3D && cp $R3D/regard/build/Regard3D /usr/local/bin
#Camera Database
RUN cp CameraSensorSizeDatabase/sensor_database.csv /usr/local/bin

# Support programs
RUN mkdir /usr/local/bin/{mve,pmvs,poisson}

RUN cd $R3D/mve/apps && \
cp dmrecon/dmrecon \
fssrecon/fssrecon \
makescene/makescene \
meshclean/meshclean \
scene2pset/scene2pset \
$R3D/mvs-texturing/build/apps/texrecon/texrecon \
$R3D/smvs/app/smvsrecon_SSE41 \
/usr/local/bin/mve

RUN cd $R3D/cmvs/program/main && \
cp cmvs \
genOption \
pmvs2 \
/usr/local/bin/pmvs

RUN cd $R3D/PoissonRecon/Bin/Linux && \
cp SurfaceTrimmer \
PoissonReco \
/usr/local/bin/poisson

